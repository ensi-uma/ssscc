#include <gtk/gtk.h>
#include <string.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include "../inc/socket.h"
#include "../inc/thread.h"

char host[32];
char port[32];
char name[32];

//declaration of GUI elements for the login window 
GtkWidget  *login_window;
GtkWidget  *input_host;
GtkWidget  *input_port;
GtkWidget  *input_name;

//declaration of GUI elements for the chat window

GtkWidget  *chat_window;
GtkWidget  *scrolled_window;
GtkWidget  *input;
GtkWidget  *output;


void receiver_thread (void *);
void output_print(char*);
void ScrollToEnd ();
void on_connect_button_click(GtkWidget*, gpointer*);
void on_chat_window_show(GtkWidget*, gpointer*);
void on_input_activate(GtkWidget*, gpointer*);
void on_window_destroy();


int main(int argc, char *argv[]) {

    gtk_init(&argc, &argv);

    // interfaces created using "Glade Interface Designer" software
    if( access("interface0.glade", R_OK) < 0 || access("interface0.glade", R_OK) < 0 ) {
    	// files dont exists
    	printf( "interface0.glade and interface1.glade (GUI files) files\n"
    			"needs to be in the same dir as the client executable\n");
    	exit(-1);
	}

	// loading the login window interface
    GtkBuilder *builder0 = gtk_builder_new();
    gtk_builder_add_from_file (builder0, "interface0.glade", NULL);
    login_window = GTK_WIDGET(gtk_builder_get_object(builder0, "window_main"));
    input_host = GTK_WIDGET(gtk_builder_get_object(builder0, "host"));
	input_port = GTK_WIDGET(gtk_builder_get_object(builder0, "port"));
	input_name = GTK_WIDGET(gtk_builder_get_object(builder0, "name"));
	gtk_builder_connect_signals(builder0, NULL);
	g_object_unref(builder0);

	// loading the chat window interface
	GtkBuilder *builder1 = gtk_builder_new();
    gtk_builder_add_from_file (builder1, "interface1.glade", NULL);
	chat_window = GTK_WIDGET(gtk_builder_get_object(builder1, "window_main"));
	scrolled_window  = GTK_WIDGET(gtk_builder_get_object(builder1, "output_window"));
	GtkAdjustment* adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW (scrolled_window));
	g_signal_connect (adj, "changed", G_CALLBACK (ScrollToEnd), NULL);
	input  = GTK_WIDGET(gtk_builder_get_object(builder1, "input"));
	output = GTK_WIDGET(gtk_builder_get_object(builder1, "output"));
	gtk_builder_connect_signals(builder1, NULL);
    g_object_unref(builder1);

    gtk_widget_show(login_window);                
    gtk_main();

    return 0;
}

//receive and print server messages 
void receiver_thread (void* arg) {
    char buffer[256];
    while(1) {
        if (socket_recv(socket_id, buffer, sizeof(buffer))) {
            output_print("Disconnected !");
            break;
        }
     	output_print(buffer);
    }
}
//printing a message on the chat output text area 
void output_print(char* in_buffer) {
	//force refresh     # this to fix a bug
	while (gtk_events_pending()) gtk_main_iteration();

	// adding time and newline;
	char buffer[256+32];
	time_t mytime = time(NULL);
    char * time_str = ctime(&mytime);
    time_str[strlen(time_str)-1] = '\0';
    snprintf(buffer, sizeof(buffer), "[%s] %s\n", time_str, in_buffer);

	// apppending text to the output
	GtkTextIter iter;
    GtkTextBuffer* text = gtk_text_view_get_buffer (GTK_TEXT_VIEW (output));
    GtkTextMark* mark = gtk_text_buffer_get_insert (text);
    gtk_text_buffer_get_iter_at_mark (text, &iter, mark);
    gtk_text_buffer_insert (text, &iter, (gchar*) buffer, -1);

    //ScrollToEnd();
}

//autoscroll to the last received message
void ScrollToEnd () {
	//force refresh     # this to fix a bug
	while (gtk_events_pending()) gtk_main_iteration();

    GtkAdjustment* adj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW (scrolled_window));
    gtk_adjustment_set_value (adj, gtk_adjustment_get_upper (adj));
}

//getting user input 
void on_connect_button_click(GtkWidget *widget, gpointer *data) {
	strcpy(host, (char*) gtk_entry_get_text((GtkEntry*) input_host));
	strcpy(port, (char*) gtk_entry_get_text((GtkEntry*) input_port));
	strcpy(name, (char*) gtk_entry_get_text((GtkEntry*) input_name));
	gtk_widget_show(chat_window);
	gtk_widget_hide(login_window);             
}
//connecting to the server on click 
void on_chat_window_show(GtkWidget *widget, gpointer *data) {
	//force refresh     # this to fix a bug
	while (gtk_events_pending()) gtk_main_iteration();

	int res;
	char title[32];
	snprintf(title, sizeof(title), "%s:%s", host, port);
	gtk_window_set_title (GTK_WINDOW(chat_window), title);
	output_print("Connecting ...");
	if ((res = socket_client(host, port)) != 0) {
		output_print(strerror(res));
		gtk_editable_set_editable(GTK_EDITABLE(input), FALSE); //disable input
	} else {
		output_print("Connected");
		thread_create(&receiver_thread, NULL);
		socket_send(socket_id, name);
		gtk_editable_set_editable(GTK_EDITABLE(input), TRUE); //activate input
	}
}

//send user input to the server
void on_input_activate(GtkWidget *widget, gpointer *data) {
	if (!socket_ready()) return;
	socket_send(socket_id, (char *)gtk_entry_get_text((GtkEntry*) input));
    gtk_entry_set_text ((GtkEntry*) input, (gchar*) "");
}

// called when window is closed
void on_window_destroy() {
	socket_close();
    gtk_main_quit();
}