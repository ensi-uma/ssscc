
Sujet         : 2 - Implémentation d’un système de chat en C sous Unix
Enseignant(e) : Imtiez Fliss
Groupe (II1D) : Yousef Fehri - Tarek Lammouchi


NOTE 1: .glade (GUI files) files needs to be in the same dir as the client executable

NOTE 2: PLEASE make sure build-essential is installed
		USE : sudo apt-get install build-essential

NOTE 3: PLEASE make sure libgtk-3-dev is installed
		USE : sudo apt-get install libgtk-3-dev

NOTE 4: there is a small bug in the client (GUI) that causes it to sometimes crash
		if that happen PLEASE restart the client
