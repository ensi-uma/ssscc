#libgtk-3-dev 

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

all: bin/server bin/client

clean:
	rm -fr bin/* obj/*

### SERVER

runserver: bin/server
	./bin/server 8765

bin/server: obj/server.o
	gcc obj/server.o -o bin/server -pthread

obj/server.o: libc-dev src/server.c inc/socket.h inc/thread.h inc/queue.h inc/mutex.h
	gcc -c src/server.c -o obj/server.o -Wall

### CLIENT

runclient: bin/client
	# setting current dir to the location of "interface0.glade" and "interface1.glade"
	cd bin/ && ./client

bin/client: obj/client.o
	gcc obj/client.o -o bin/client -pthread $(GTKLIB) -export-dynamic
	cp src/interface0.glade src/interface1.glade bin/ # .glade needs to be in the same dir as executable

obj/client.o: libc-dev src/client.c inc/socket.h inc/thread.h libc-dev libgtk-3-dev
	gcc -c src/client.c -o obj/client.o -Wall $(GTKLIB)

libc-dev: 
	@echo "*****************************************************"
	@echo "*** PLEASE make sure build-essential is installed ***"
	@echo "***  USE : sudo apt-get install build-essential   ***"
	@echo "*****************************************************"

libgtk-3-dev: 
	@echo "***  PLEASE make sure libgtk-3-dev is installed   ***"
	@echo "***   USE : sudo apt-get install libgtk-3-dev     ***"
	@echo "*****************************************************"
